import readerService from "../services/reader.service";

const ReaderRoutes = (router) => {
  router.get("/", async (req, res) => {
    try {
      const readers = await readerService.getAllReaders();
      res.status(200).send(readers);
    } catch (err) {
      res.status(500).send(err);
    }
  });

  router.get("/lazy", async (req, res) => {
    try {
      const readers = await readerService.getLazyReaders();
      res.status(200).send(readers);
    } catch (err) {
      res.status(500).send(err);
    }
  });

  router.get("/:id", async (req, res) => {
    try {
      const reader = await readerService.getReaderById(req.params.id);
      res.status(200).send(reader);
    } catch (err) {
      res.status(500).send(err);
    }
  });

  router.put("/", async (req, res) => {
    try {
      const newReader = await readerService.addReader(req.body);
      res.status(200).send(newReader);
    } catch (err) {
      res.status(500).send(err);
    }
  });

  router.delete("/:id", async (req, res) => {
    try {
      const reader = await readerService.deleteReader(req.params.id);
      res.status(200).send(reader);
    } catch (err) {
      res.status(500).send(err);
    }
  });

  return router;
};

export default ReaderRoutes;
