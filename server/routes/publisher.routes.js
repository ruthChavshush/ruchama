import publisherService from "../services/publisher.service";

const PublisherRoutes = (router) => {
  router.get("/", async (req, res) => {
    try {
      const publishers = await publisherService.getAllPublishers();
      res.status(200).send(publishers);
    } catch (err) {
      res.status(500).send(err)
    }
  });

  router.get("/:id", async (req, res) => {
    try {
      const publisher = await publisherService.getPublisherById(req.params.id);
      res.status(200).send(publisher);
    } catch (err) {
      res.status(500).send(err)
    }
  });

  router.get("/payment/:id", async (req, res) => {
    try {
      const payment = await publisherService.getPaymentForPublisherById(req.params.id);
      res.status(200).send(`total payment is ${payment}`);
    } catch (err) {
      res.status(500).send(err)
    }
  });

  router.put("/", async (req, res) => {
    try {
      const newPublisher = await publisherService.addPublisher(req.body);
      res.status(200).send(newPublisher);
    } catch (err) {
      res.status(500).send(err)
    }
  });

  router.delete("/:id", async (req, res) => {
    try {
      const publisher =  await publisherService.deletePublisher(req.params.id);
      res.status(200).send(publisher);
    } catch (err) {
      res.status(500).send(err);
    }
  });

  return router;
};

export default PublisherRoutes;

