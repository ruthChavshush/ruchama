import BorrowService from "../services/borrow.service";
import ReaderService from "../services/reader.service";
import BookToBorrowService from "../services/bookToBorrow.service";

const BorrowRoutes = (router) => {
  router.get("/", async (req, res) => {
    try {
      const borrows = await BorrowService.getAllBorrows();
      res.status(200).send(borrows);
    } catch (err) {
      res.status(500).send(err);
    }
  });

  router.get("/:id", async (req, res) => {
    try {
      const borrow = await BorrowService.getBookById(req.params.id);
      res.status(200).send(borrow);
    } catch (err) {
      res.status(500).send(err);
    }
  });

  router.post("/finish/:id", async (req, res) => {
    try {
      const borrow = await BorrowService.finishBorrow(req.params.id);
      res.status(200).send(borrow);
    } catch (err) {
      res.status(500).send(err);
    }
  });

  router.put("/", async (req, res) => {
    try {
      const reader = await ReaderService.getReaderByIdNumber(req.body.readerId);
      if (reader == null) {
        throw "no such reader with id number found";
      }
      const bookToBorrowId =
        await BookToBorrowService.getAvailableBookToBorrowId(req.body.bookId);
      if (bookToBorrowId == -1) {
        throw "no available book found";
      }
      const newBorrow = await BorrowService.addBorrow({
        readerId: reader.id,
        bookId: bookToBorrowId,
      });
      res.status(200).send(newBorrow);
    } catch (err) {
      res.status(500).send(err);
    }
  });

  return router;
};

export default BorrowRoutes;
