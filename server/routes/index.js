import { Router } from "express"
import fs from "fs"
import { fileURLToPath, pathToFileURL } from "url"
import { dirname, join } from "path"

const __filename = fileURLToPath(import.meta.url)
const __dirname = dirname(__filename)

const routes = async (app) => {
  const files = fs.readdirSync(__dirname)

  for (const file of files) {
    if (file === "index.js") continue

    const router = Router()
    const module = await import(pathToFileURL(join(__dirname, file)))
    const routeModule = module.default
    const path = (routeModule.path || "/" + file.replace(".routes.js", ""))
    const route = routeModule.config
      ? routeModule.config(router)
      : routeModule(router)
    app.use(path, route)
  }
}

export default routes