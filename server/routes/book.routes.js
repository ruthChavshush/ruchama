import bookService from "../services/book.service";
import BookService from "../services/book.service";
import BookToBorrowService from "../services/bookToBorrow.service";

const BookRoutes = (router) => {
  router.get("/", async (req, res) => {
    try {
      const books = await BookService.getAllBooks();
      res.status(200).send(books);
    } catch (err) {
      res.status(500).send(err)
    }
  });

  router.get("/:id", async (req, res) => {
    try {
      const book = await BookService.getBookById(req.params.id);
      res.status(200).send(book);
    } catch (err) {
      res.status(500).send(err)
    }
  });

  router.get("/copies/:id", async (req, res) => {
    try {
      const copies = await BookService.getCopiesNumberForBookById(req.params.id);
      res.status(200).send(`${copies}`);
    } catch (err) {
      res.status(500).send(err)
    }
  });

  router.put("/", async (req, res) => {
    try {
      const newBook = await BookService.addBook(req.body);
      const copies = await BookToBorrowService.addBookCopies(
        newBook,
        req.body.CopiesNumber
      );
      res.status(200).send(newBook);
    } catch (err) {
      res.status(500).send(err)
    }
  });

  router.get("/search/:searchQuery", async (req, res) => {
    try {
      const books = await BookService.getFilteredBooks(req.params.searchQuery);
      res.status(200).send(books);
    } catch (err) {
      res.status(500).send(err)
    }
  });

  router.get("/borrow/available", async (req, res) => {
    try {
      const books = await BookService.getBooksForBorrow();
      res.status(200).send(books);
    } catch (err) {
      res.status(500).send(err)
    }
  });

  router.get("/bestSellers/:count", async (req, res) => {
    try {
      const books = await BookService.getBestSellers(req.params.count);
      res.status(200).send(books);
    } catch (err) {
      res.status(500).send(err)
    }
  });

  router.delete("/:id", async (req, res, next) => {
    try {
      const book = await bookService.deleteBook(req.params.id);
      res.status(200).send(book);
    } catch (err) {
      res.status(500).send(err);
    }
  });

  return router;
};

export default BookRoutes;
