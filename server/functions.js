
import moment from "moment";

export const currentDate = ()=>{
    return moment().tz("Asia/Jerusalem").format("YYYY-MM-DD HH:mm:ss");
}