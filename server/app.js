import cors from "cors";
import Express from "express";
import routes from "./routes";
import modelsManager from "./modelsManager";

const expressApp = async (app) => {
  app.use(Express.json());
  app.use(cors());
  await modelsManager.generate();
  await routes(app);
};

export default expressApp;
