import "dotenv/config";
import expressApp from "./app";
import Express from "express";

const server = () => {
  const app = Express();
  expressApp(app);
  app.listen("1234", () => console.log("server listening..."));
};

server();
