export default (sequelize, DataTypes) => {
  const LibReader = sequelize.define(
    "LibReader",
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true,
      },
      id_number: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      first_name: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      last_name: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      phone_number: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      closed_date:{
        type: DataTypes.DATE,
        allowNull: true,
      }
    },
    {
      tableName: "lib_reader",
    }
  );

  LibReader.associate = (models) => {
    LibReader.hasMany(models.LibBorrow, {
      foreignKey: "reader_id",
    });
  };

  return LibReader;
};
