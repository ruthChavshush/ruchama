

export default (sequelize, DataTypes) => {
  const LibBorrow = 
    sequelize.define("LibBorrow", {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true,
        field: "id",
      },
      borrow_date: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      return_date: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      book_to_borrow_id:{
        type: DataTypes.INTEGER
      },
      reader_id:{
        type: DataTypes.INTEGER
      }
    }
    ,
    {
      tableName: "lib_borrow",
    });

    LibBorrow.associate = (models) => {
      LibBorrow.belongsTo(models.LibReader, {
        foreignKey: "reader_id",
      }),
      LibBorrow.belongsTo(models.LibBookToBorrow, {
        foreignKey: "book_to_borrow_id",
      })
      ;
    }
    return LibBorrow;
 
};
