export default (sequelize, DataTypes) => {
  const LibBookToBorrow = sequelize.define(
    "LibBookToBorrow",
    {
      book_to_borrow_id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true,
        field: "id",
      },
      book_id: {
        type: DataTypes.INTEGER,
      },
      closed_date: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      tableName: "lib_book_to_borrow",
    }
  );

  LibBookToBorrow.associate = (models) => {
    LibBookToBorrow.hasMany(models.LibBorrow, {
      foreignKey: "book_to_borrow_id",
    });
    LibBookToBorrow.belongsTo(models.LibBook, {
      foreignKey: "book_id",
    });
  };

  return LibBookToBorrow;
};
