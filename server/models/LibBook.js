export default (sequelize, DataTypes) => {
  const LibBook = sequelize.define(
    "LibBook",
    {
      book_id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true,
        field: "id",
      },
      name: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      cover_img_url: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      price: {
        type: DataTypes.FLOAT,
        allowNull: false,
      },
      closed_date:{
        type: DataTypes.DATE,
        allowNull:true,
    }
    },
    {
      tableName: "lib_book",
    }
  );

  LibBook.associate = (models) => {
    LibBook.belongsTo(models.LibPublisher, {
      foreignKey: "publisher_id",
    });
    LibBook.hasMany(models.LibBookToBorrow, {
      foreignKey: "book_id",
    });
  };

  return LibBook;
};
