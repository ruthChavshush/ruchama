

export default (sequelize, DataTypes) => {
    const LibPublisher = sequelize.define(
      "LibPublisher",
      {
        id:{
            type: DataTypes.INTEGER,
            autoIncrement:true,
            allowNull:false,
            primaryKey:true
        },
        id_number:{
            type: DataTypes.STRING,
            allowNull:true,
        },
        first_name:{
            type: DataTypes.STRING,
            allowNull:true,
        },
        last_name:{
            type: DataTypes.STRING,
            allowNull:true,
        },
        closed_date:{
            type: DataTypes.DATE,
            allowNull:true,
        }
      },
      {
        tableName: "lib_publisher",
      }
    );
  
    LibPublisher.associate = (models) => {
        LibPublisher.hasMany(models.LibBook, {
          foreignKey: "publisher_id",
        })
    };
  
    return LibPublisher;
  };
  

