import Sequelize from "sequelize";
import sequelize from "./sequelize";
import { pathToFileURL, fileURLToPath } from "url";
import { join, dirname } from "path";
import fs from "fs";

const __filename = fileURLToPath(import.meta.url);
const _dirname = dirname(__filename);
const modules = [];
const models = [];
const path = `${_dirname}/models`;

const generate = async () => {
  const files = fs.readdirSync(path);

  for (const file of files) {
    const model = await import(pathToFileURL(join(path, file)));
    modules.push(model.default);
  }

  for (const module of modules) {
    const model = module(sequelize, Sequelize.DataTypes);
    models[model.name] = model;
  }

  Object.keys(models).forEach((model) => {
    if (models[model].associate) {
      models[model].associate(models);
    }
  });

  return models;
};

export default { generate, models };
