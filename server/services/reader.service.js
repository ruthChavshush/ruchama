import modelsManager from "../modelsManager";
import Sequelize,{ Op } from "sequelize";
import moment from "moment";
import { currentDate } from "../functions";

const getAllReaders = async () => {
  const readers = await modelsManager.models.LibReader.findAll({
    where: {
      closed_date: {
        [Op.is]: null,
      },
    },
  });
  return readers;
};

const getLazyReaders = async () => {
  const earlier = moment().subtract(2, "weeks");
  const readers = await modelsManager.models.LibBorrow.findAll({
    attributes: [
      [Sequelize.fn("DISTINCT", Sequelize.col("reader_id")), "reader_id"],
    ],
    where: {
      return_date: {
        [Op.is]: null,
      },
      borrow_date: {
        [Op.lte]: earlier
      },
    },
  });
  const ids = readers.map((reader) => reader.dataValues.reader_id);
  const lazyReader = await modelsManager.models.LibReader.findAll({
    where: {
      id: {
        [Op.in]: ids,
      },
    },
  });

  return lazyReader;
};

const getReaderById = async (id) => {
  const reader = await modelsManager.models.LibReader.findOne({
    where: {
      id: id,
    },
  });

  return reader;
};

const getReaderByIdNumber = async (idNumber) => {
  const reader = await modelsManager.models.LibReader.findOne({
    where: {
      id_number: idNumber,
    },
  });

  return reader;
};

const addReader = async (readerToAdd) => {
  const reader = await modelsManager.models.LibReader.create({
    id_number: readerToAdd.id,
    first_name: readerToAdd.firstName,
    last_name: readerToAdd.lastName,
    phone_number: readerToAdd.phone,
  });

  return reader;
};

const isDeleteValid = async (id) => {
  const readerBorrows = await modelsManager.models.LibBorrow.findAll({
    where: {
      reader_id: id,
      return_date: {
        [Op.is]: null,
      },
    },
  });

  return readerBorrows.length == 0;
};

const deleteReader = async (id) => {
  try {
    if (await isDeleteValid(id)) {
      const reader = await getReaderById(id);
      reader.closed_date = currentDate();
      await reader.save();
      return reader;
    } else {
      throw "can't delete reader. active borrows exist.";
    }
  } catch (err) {
    throw err;
  }
};

export default {
  getAllReaders,
  getReaderById,
  getReaderByIdNumber,
  deleteReader,
  getLazyReaders,
  addReader,
};
