import modelsManager from "../modelsManager";
import sequelize from "../sequelize";

const addBookCopies = async (bookToAdd, copiesNumber) => {
  let booksToBorrow = [];
  for (let i = 0; i < copiesNumber; i++) {
    const book = await modelsManager.models.LibBookToBorrow.create({
      book_id: bookToAdd.book_id,
    });

    booksToBorrow.push(book);
  }
  return booksToBorrow;
};

const getAvailableBookToBorrowId = async (bookId) => {
  const query = `select top 1 * 
                    from lib_book_to_borrow b
                    where book_id=${bookId} and 
                         (select count(*) 
                         from lib_borrow b2 
                         where b2.book_to_borrow_id=b.id  
                            and b2.return_date is null ) = 0`;

  const res = await sequelize.query(query);
  return res[1] == 0 ? -1 : res[0][0].id;
};

export default { addBookCopies, getAvailableBookToBorrowId };
