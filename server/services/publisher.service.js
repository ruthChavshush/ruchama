import modelsManager from "../modelsManager";
import { Op } from "sequelize";
import { currentDate } from "../functions";
import BookService from "./book.service";

const getAllPublishers = async () => {
  const publishers = await modelsManager.models.LibPublisher.findAll({
    where: {
      closed_date: {
        [Op.is]: null,
      },
    },
  });
  return publishers;
};

const getPublisherById = async (id) => {
  const publisher = await modelsManager.models.LibPublisher.findOne({
    where: {
      id: id,
    },
  });
  return publisher;
};

const addPublisher = async (publisherToAdd) => {
  const publisher = await modelsManager.models.LibPublisher.create({
    id_number: publisherToAdd.id,
    first_name: publisherToAdd.firstName,
    last_name: publisherToAdd.lastName,
  });
  return publisher;
};

const isDeleteValid = async (id) => {
  const publisherBooks = await modelsManager.models.LibBook.findAll({
    where: {
      closed_date: {
        [Op.is]: null,
      },
      publisher_id: id,
    },
  });

  return publisherBooks.length == 0;
};

const deletePublisher = async (id) => {
  try {
    if (await isDeleteValid(id)) {
      const publisher = await getPublisherById(id);
      publisher.closed_date = currentDate();
      await publisher.save();
      return publisher;
    } else {
      throw "can't delete publisher. its books in use.";
    }
  } catch (err) {
    throw err;
  }
};

const getPaymentForPublisherById = async (id) => {
  const books = await BookService.getBooksByPublisherId(id);
  let totalPayment = 0;
  for (const book of books) {
    const copies = await BookService.getCopiesNumberForBookById(book.book_id);
    const price = (await book.price) * copies;
    totalPayment += await price;
  }

  return totalPayment;
};

export default {
  getAllPublishers,
  getPublisherById,
  addPublisher,
  deletePublisher,
  getPaymentForPublisherById,
};
