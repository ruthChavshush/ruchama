import modelsManager from "../modelsManager";
import { Op } from "sequelize";
import sequelize from "../sequelize";
import { currentDate } from "../functions";
import BookToBorrowService from "./bookToBorrow.service";

const getAllBooks = async () => {
  const books = await modelsManager.models.LibBook.findAll({
    include: modelsManager.models.LibPublisher,
    where: {
      closed_date: {
        [Op.is]: null,
      },
    },
  });
  return books;
};

const getBooksForBorrow = async () => {
  const books = await getAllBooks();
  const availableBooks = await books.filter(
    async (book) =>
      (await BookToBorrowService.getAvailableBookToBorrowId(book.book_id)) != -1
  );
  return availableBooks;
};

const getBookById = async (id) => {
  const book = await modelsManager.models.LibBook.findOne({
    where: {
      id: id,
    },
  });
  return book;
};

const getBooksByPublisherId = async (id) => {
  const books = await modelsManager.models.LibBook.findAll({
    where: {
      publisher_id: id,
    },
  });
  return books;
};

const addBook = async (bookToAdd) => {
  const book = await modelsManager.models.LibBook.create({
    name: bookToAdd.bookName,
    publisher_id: bookToAdd.PublisherId,
    cover_img_url: bookToAdd.CoverImageUrl,
    price: bookToAdd.price,
  });
  return book;
};

const getFilteredBooks = async (searchQuery) => {
  const books = await modelsManager.models.LibBook.findAll({
    include: modelsManager.models.LibPublisher,
    where: {
      closed_date: {
        [Op.is]: null,
      },
      name: {
        [Op.like]: "%" + searchQuery + "%",
      },
    },
  });
  return books;
};

const getBestSellers = async (count) => {
  const bestSellers = await sequelize.query(`SELECT *
  FROM   lib_book books
         JOIN (SELECT bookToBorrow.book_id,
                             Count(*) AS borrowsCount
               FROM   lib_borrow borrow
                      JOIN lib_book_to_borrow bookToBorrow
                        ON borrow.book_to_borrow_id = bookToBorrow.id
               GROUP  BY bookToBorrow.book_id) bestSellers
           ON books.id = bestSellers.book_id
  ORDER  BY bestSellers.borrowsCount DESC `);
  return bestSellers[0].slice(0, count);
};

const getCopiesNumberForBookById = async (id) => {
  const copies = await modelsManager.models.LibBookToBorrow.count({
    where: { book_id: id },
  });
  return copies;
};

const getPaymentForBookById = async (id) => {
  const book = await getBookById(id);
  const copies = await getCopiesNumberForBookById(id);
  return book.price * copies;
};

const isDeleteValid = async (id) => {
  const borrowedBooks = await modelsManager.models.LibBorrow.findAll({
    attributes: ["book_to_borrow_id"],
    include: {
      attributes: [],
      model: modelsManager.models.LibBookToBorrow,
      required: true,
      include: {
        model: modelsManager.models.LibBook,
        attributes: [],
        required: true,
        where: {
          id: id,
        },
      },
    },
    where: {
      return_date: {
        [Op.is]: null,
      },
    },
  });

  return borrowedBooks.length == 0;
};

const deleteBookCopies = async (id) => {
  try {
    const date = currentDate();
    await modelsManager.models.LibBookToBorrow.update(
      { closed_date: date },
      {
        where: {
          book_id: id,
        },
      }
    );
  } catch (err) {
    throw err;
  }
};

const deleteBook = async (id) => {
  try {
    if (await isDeleteValid(id)) {
      const book = await getBookById(id);
      await deleteBookCopies(id);
      book.closed_date = currentDate();
      await book.save();
      return book;
    } else {
      throw "can't delete book. its copies in use.";
    }
  } catch (err) {
    throw err;
  }
};

export default {
  getAllBooks,
  getBooksForBorrow,
  getBookById,
  addBook,
  getFilteredBooks,
  getBestSellers,
  getBooksByPublisherId,
  getPaymentForBookById,
  getCopiesNumberForBookById,
  deleteBook,
};
