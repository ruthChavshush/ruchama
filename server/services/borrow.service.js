import modelsManager from "../modelsManager";
import { Op } from "sequelize";
import { currentDate } from "../functions";

const getAllBorrows = async () => {
  const borrows = await modelsManager.models.LibBorrow.findAll({
    include: [
      {
        model: modelsManager.models.LibBookToBorrow,
        include: modelsManager.models.LibBook,
      },
      modelsManager.models.LibReader,
    ],
    where: {
      return_date: {
        [Op.is]: null,
      },
    },
  });
  return borrows;
};

const getBorrowById = async (id) => {
  const borrow = await modelsManager.models.LibBorrow.findOne({
    include: modelsManager.models.LibBook,
    include: modelsManager.models.LibReader,
    where: {
      id: id,
    },
  });

  return borrow;
};

const addBorrow = async (borrowToAdd) => {
  let borrow = await modelsManager.models.LibBorrow.create({
    book_to_borrow_id: borrowToAdd.bookId,
    reader_id: borrowToAdd.readerId,
    borrow_date: currentDate(),
  });

  borrow = await modelsManager.models.LibBorrow.findOne({
    include: modelsManager.models.LibReader,
    where: {
      id: borrow.id,
    },
  });

  return borrow;
};

const finishBorrow = async (id) => {
  try {
    let borrow = await getBorrowById(id);
    borrow.return_date = currentDate();
    await borrow.save();
    borrow = await modelsManager.models.LibBorrow.findOne({
      include: modelsManager.models.LibReader,
      include: {
        model: modelsManager.models.LibBookToBorrow,
        include: modelsManager.models.LibBook,
      },
      where: {
        id: borrow.id,
      },
    });

    return borrow;
  } catch (err) {
    throw err;
  }
};

export default { getAllBorrows, getBorrowById, addBorrow, finishBorrow };
