const props = [
  {
    type: "txt",
    realName: "id",
    displayName: "#",
  },
  {
    type: "txt",
    realName: "readerName",
    displayName: "Reader",
  },
  {
    type: "txt",
    realName: "bookName",
    displayName: "Book",
  },
  {
    type: "txt",
    realName: "borrow_date",
    displayName: "Date",
  },
];

export default { props };
