const props = [
  {
    type: "txt",
    realName: "id",
    displayName: "#",
  },
  {
    type: "txt",
    realName: "id_number",
    // "realName":"book_to_borrow_id",
    displayName: "ID Number",
  },
  {
    type: "txt",
    realName: "first_name",
    displayName: "First Name",
  },
  {
    type: "txt",
    realName: "last_name",
    displayName: "Last Name",
  },
  {
    type: "txt",
    realName: "phone_number",
    displayName: "Phone",
  },
  // {
  //   type: "img",
  //   realName: "../img/library-background.jpg",
  //   displayName: "image",
  //   alt:"book image"
  // }
];

export default { props };
