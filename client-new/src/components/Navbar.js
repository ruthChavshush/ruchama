import React from "react";
import { AppBar, Toolbar, Button, IconButton } from "@mui/material";
import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";
import HomeIcon from "@mui/icons-material/Home";
/** @jsxImportSource @emotion/react */
import { css } from "@emotion/react";

const Navbar = () => {
  return (
    <nav>
      <AppBar
        position="fixed"
        css={css({
          backgroundColor: "#f5f5f5",
          height: "10vh",
          minHeight: "50px",
          display: "flex",
          flexDirection: "row",
          flexWrap: "no-wrap",
          justifyContent: "left",
          alignItems: "center",
          backgroundColor: "#e0e0e0",
          opacity: "0.8",
        })}
      >
        <Toolbar>
          <Link to="/">
            {" "}
            <IconButton
              size="large"
              edge="start"
              color="inherit"
              aria-label="menu"
              sx={{ mr: 2 }}
            >
              <HomeIcon />
            </IconButton>
          </Link>

          <Link to="/readers" css={css({ underline: "none" })}>
            {" "}
            <Button color="inherit">Readers</Button>
          </Link>
          <Link to="/books" underline="none">
            {" "}
            <Button color="inherit">Books</Button>
          </Link>
          <Link to="/publishers">
            {" "}
            <Button color="inherit">Publishers</Button>
          </Link>
          <Link to="/borrows">
            {" "}
            <Button color="inherit">Borrows</Button>
          </Link>
        </Toolbar>
      </AppBar>
    </nav>
  );
};

export default Navbar;
