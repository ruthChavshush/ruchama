import React, { useState, useEffect } from "react";
import Table from "../generic/BasicTable";
import { Button, CircularProgress, Box } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import { Link } from "react-router-dom";
import BorrowProps from "../tableProps/BorrowProps";
import axios from "axios";
import FinishBorrowIcon from "@mui/icons-material/SyncAlt";
import Dialog from "../generic/Dialog";
import Snackbar from "../generic/Snackbar";
/** @jsxImportSource @emotion/react */
import { css } from "@emotion/react";

const Borrows = () => {
  const [borrows, setBorrows] = useState([]);
  const [selectedBorrowId, setSelectedBorrowId] = useState({});
  const [isLoading, setIsLoading] = useState(true);
  const [isSnackBarOpen, setIsSnackBarOpen] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState();
  const [snackbarSeverity, setSnackbarSeverity] = useState();
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const [dialogContent, setDialogContent] = useState();
  const [dialogTitle, setDialogTitle] = useState();

  const getAllBorrows = async () => {
    let res = await axios.get("http://localhost:1234/borrow");
    setBorrows(res.data);
    setIsLoading(false);
  };

  useEffect(() => {
    getAllBorrows();
  }, []);

  const finishBorrow = async () => {
    try {
      let res = await axios.post(
        `http://localhost:1234/borrow/finish/${selectedBorrowId}`
      );
      setIsLoading(true);
      getAllBorrows();
      setSnackbarMessage(`${res.data.LibBookToBorrow.LibBook.name} returned`);
      setSnackbarSeverity("success");
    } catch (err) {
      setSnackbarMessage(err.response.data || err.message);
      setSnackbarSeverity("error");
    } finally {
      closeDialog();
      openSnackBar();
    }
  };

  const handleReturn = async (event) => {
    setSelectedBorrowId(event.currentTarget.id);
    setDialogTitle("Are you sure?");
    const borrow = await borrowsRows.find(borrow=> borrow.id == event.currentTarget.id);
    setDialogContent(`return  ${borrow.bookName}?`);
    setIsDialogOpen(true);
  };

  const openSnackBar = () => {
    setIsSnackBarOpen(true);
  };

  const closeSnackBar = () => {
    setIsSnackBarOpen(false);
  };

  const closeDialog = () => {
    setIsDialogOpen(false);
  };

  const returnBookAction = {
    icon: FinishBorrowIcon,
    text: "return book",
    action: handleReturn,
  };

  const borrowsRows = borrows.map(borrow=> {
    return {...borrow, 
      readerName: `${borrow.LibReader.first_name} ${borrow.LibReader.last_name}`,
      bookName: `${borrow.LibBookToBorrow.LibBook.name}`
      
  }});

  return (
    <div
      css={css({
        display: "flex",
        flexDirection: "column",
        flexWrap: "wrap",
        justifyContent: "top",
        alignItems: "center",
        padding: "100px",
      })}
    >
      <h1>Borrows</h1>
      {isLoading && (
        <Box sx={{ display: "flex" }}>
          <CircularProgress />
        </Box>
      )}
      {!isLoading && (
        <Table
          actions={[returnBookAction]}
          headers={BorrowProps.props}
          rows={borrowsRows}
        />
      )}
      <Snackbar
        message={snackbarMessage}
        open={isSnackBarOpen}
        handleClose={closeSnackBar}
        severity={snackbarSeverity}
      />
      <Dialog
        title={dialogTitle}
        content={dialogContent}
        open={isDialogOpen}
        handleClose={closeDialog}
        handleOK={finishBorrow}
        handleCancel={closeDialog}
      />
      <Button
        component={Link}
        to="/newBorrow"
        variant="contained"
        startIcon={<AddIcon />}
        css={css({
          margin: 10,
          bottom: "7%",
          left: "3%",
          position: "fixed",
        })}
      >
        New Borrow
      </Button>
    </div>
  );
};

export default Borrows;
