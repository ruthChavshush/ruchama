import { React, useState, useEffect } from "react";
import Table from "../generic/BasicTable";
import { Button, CircularProgress, Box } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import { Link } from "react-router-dom";
import axios from "axios";
import Modal from "../generic/BasicModal";
import DeleteIcon from "@mui/icons-material/Delete";
import PaymentIcon from "@mui/icons-material/Payment";
import PublisherProps from "../tableProps/PublisherProps";
import Snackbar from "../generic/Snackbar";
import Dialog from "../generic/Dialog";
/** @jsxImportSource @emotion/react */
import { css } from "@emotion/react";

const Publishers = () => {
  const [publishers, setPublishers] = useState([]);
  const [selectedPublisherId, setSelectedPublisherId] = useState();
  const [isLoading, setIsLoading] = useState(true);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [modalTitle, setModalTitle] = useState("");
  const [modalContent, setModalContent] = useState();
  const [isSnackBarOpen, setIsSnackBarOpen] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState();
  const [snackbarSeverity, setSnackbarSeverity] = useState();
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const [dialogContent, setDialogContent] = useState();
  const [dialogTitle, setDialogTitle] = useState();

  const getAllPublishers = async () => {
    let res = await axios.get("http://localhost:1234/publisher");
    setPublishers(res.data);
    setIsLoading(false);
  };

  useEffect(() => {
    getAllPublishers();
  }, []);

  const deletePublisher = async () => {
    try {
      let res = await axios.delete(
        `http://localhost:1234/publisher/${selectedPublisherId}`
      );
      setIsLoading(true);
      getAllPublishers();
      setSnackbarMessage(
        `${res.data.first_name} ${res.data.last_name} deleted`
      );
      setSnackbarSeverity("success");
    } catch (err) {
      setSnackbarMessage(err.response.data || err.message);
      setSnackbarSeverity("error");
    } finally {
      closeDialog();
      openSnackBar();
    }
  };

  const handleDelete = async (event) => {
    setSelectedPublisherId(event.currentTarget.id);
    setDialogTitle("Are you sure?");
    const publisher = await publishers.find(publisher=> publisher.id == event.currentTarget.id);
    setDialogContent(`delete ${publisher.first_name} ${publisher.last_name}?`);
    setIsDialogOpen(true);
  };

  const handlePayment = async (event) => {
    const id = await event.currentTarget.id;
    let res = await axios.get(`http://localhost:1234/publisher/payment/${id}`);
    const publisher = await publishers.find(publisher=> publisher.id == id);
    setModalTitle(`Payment For ${publisher.first_name} ${publisher.last_name}`);
    setModalContent(res.data);
    openModal();
  };

  const openModal = () => {
    setIsModalOpen(true);
  };

  const clodeModal = () => {
    setIsModalOpen(false);
  };

  const closeDialog = () => {
    setIsDialogOpen(false);
  };

  const openSnackBar = () => {
    setIsSnackBarOpen(true);
  };

  const closeSnackBar = () => {
    setIsSnackBarOpen(false);
  };

  const deleteAction = {
    icon: DeleteIcon,
    text: "delete",
    action: handleDelete,
  };

  const payAction = {
    icon: PaymentIcon,
    text: "pay",
    action: handlePayment,
  };

  return (
    <div
      css={css({
        display: "flex",
        flexDirection: "column",
        flexWrap: "wrap",
        justifyContent: "top",
        alignItems: "center",
        padding: "100px",
      })}
    >
      <h1>Publishers</h1>
      {isLoading && (
        <Box sx={{ display: "flex" }}>
          <CircularProgress />
        </Box>
      )}
      {!isLoading && (
        <div>
          <Table
            actions={[deleteAction, payAction]}
            headers={PublisherProps.props}
            rows={publishers}
          />
          <Modal
            title={modalTitle}
            content={modalContent}
            open={isModalOpen}
            handleOpen={openModal}
            handleClose={clodeModal}
          />
        </div>
      )}
      <Snackbar
        message={snackbarMessage}
        open={isSnackBarOpen}
        handleClose={closeSnackBar}
        severity={snackbarSeverity}
      />
      <Dialog
        title={dialogTitle}
        content={dialogContent}
        open={isDialogOpen}
        handleClose={closeDialog}
        handleOK={deletePublisher}
        handleCancel={closeDialog}
      />
      <Button
        component={Link}
        to="/newPublisher"
        variant="contained"
        startIcon={<AddIcon />}
        css={css({
          margin: 10,
          bottom: "7%",
          left: "3%",
          position: "fixed",
        })}
      >
        New Publisher
      </Button>
    </div>
  );
};

export default Publishers;
