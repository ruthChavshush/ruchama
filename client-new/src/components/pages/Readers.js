import { React, useEffect, useState } from "react";
import Table from "../generic/BasicTable";
import {
  Button,
  CircularProgress,
  Radio,
  RadioGroup,
  FormControlLabel,
  FormControl,
  FormLabel,
  Box,
} from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import { Link } from "react-router-dom";
import ReaderProps from "../tableProps/ReaderProps";
import DeleteIcon from "@mui/icons-material/Delete";
import axios from "axios";
import Snackbar from "../generic/Snackbar";
import Dialog from "../generic/Dialog";
/** @jsxImportSource @emotion/react */
import { css } from "@emotion/react";

const Readers = (props) => {
  const [readers, setReaders] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isSnackBarOpen, setIsSnackBarOpen] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState();
  const [snackbarSeverity, setSnackbarSeverity] = useState();
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const [dialogContent, setDialogContent] = useState();
  const [dialogTitle, setDialogTitle] = useState();
  const [selectedReaderId, setSelectedReaderId] = useState({});

  const getAllReaders = async () => {
    setIsLoading(true);
    let res = await axios.get("http://localhost:1234/reader");
    setReaders(res.data);
    setIsLoading(false);
  };

  useEffect(() => {
    getAllReaders();
  }, []);

  const openSnackBar = () => {
    setIsSnackBarOpen(true);
  };

  const closeSnackBar = () => {
    setIsSnackBarOpen(false);
  };

  const closeDialog = () => {
    setIsDialogOpen(false);
  };
  const getLazyReaders = async () => {
    setIsLoading(true);
    let res = await axios.get("http://localhost:1234/reader/lazy");
    setReaders(res.data);
    setIsLoading(false);
  };

  const deleteReader = async (id) => {
    try {
      let res = await axios.delete(
        `http://localhost:1234/reader/${selectedReaderId}`
      );
      setIsLoading(true);
      getAllReaders();
      setSnackbarMessage(
        `${res.data.first_name} ${res.data.last_name} deleted`
      );
      setSnackbarSeverity("success");
    } catch (err) {
      setSnackbarMessage(err.response.data || err.message);
      setSnackbarSeverity("error");
    } finally {
      closeDialog();
      openSnackBar();
    }
  };

  const handleDelete = async (event) => {
    setSelectedReaderId(event.currentTarget.id);
    setDialogTitle("Are you sure?");
    const reader = await readers.find(reader=> reader.id == event.currentTarget.id);
    setDialogContent(`delete ${reader.first_name} ${reader.last_name}?`);
    setIsDialogOpen(true);
  };

  const handleRadioChange = (event) => {
    switch (event.currentTarget.value) {
      case "all":
        getAllReaders();

        break;
      case "lazy":
        getLazyReaders();

        break;
    }
  };

  const deleteAction = {
    icon: DeleteIcon,
    text: "delete",
    action: handleDelete,
  };

  return (
    <div
      css={css({
        display: "flex",
        flexDirection: "column",
        flexWrap: "wrap",
        justifyContent: "top",
        alignItems: "center",
        padding: "100px",
      })}
    >
      <h1>Readers</h1>
      {isLoading && (
        <Box sx={{ display: "flex" }}>
          <CircularProgress />
        </Box>
      )}
      {!isLoading && (
        <Table
          actions={[deleteAction]}
          headers={ReaderProps.props}
          rows={readers}
        />
      )}
      <Snackbar
        message={snackbarMessage}
        open={isSnackBarOpen}
        handleClose={closeSnackBar}
        severity={snackbarSeverity}
      />
      <Dialog
        title={dialogTitle}
        content={dialogContent}
        open={isDialogOpen}
        handleClose={closeDialog}
        handleOK={deleteReader}
        handleCancel={closeDialog}
      />
      <form
        css={css({
          margin: 10,
          top: "10%",
          left: "3%",
          position: "fixed",
          padding: "7px",
          backgroundColor: "white",
        })}
      >
        <FormControl>
          <RadioGroup
            aria-labelledby="demo-controlled-radio-buttons-group"
            name="controlled-radio-buttons-group"
            onChange={handleRadioChange}
          >
            <FormControlLabel
              css={css({
                display: "inline",
              })}
              value="all"
              control={<Radio />}
              label="ahow all readers"
            />
            <FormControlLabel
              css={css({
                display: "inline",
              })}
              value="lazy"
              control={<Radio />}
              label="show lazy readers"
            />
          </RadioGroup>
        </FormControl>
      </form>
      <Button
        component={Link}
        to="/newReader"
        variant="contained"
        startIcon={<AddIcon />}
        css={css({
          margin: 10,
          bottom: "7%",
          left: "3%",
          position: "fixed",
        })}
      >
        New Reader
      </Button>
    </div>
  );
};

export default Readers;
