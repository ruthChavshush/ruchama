/** @jsxImportSource @emotion/react */
import { css } from "@emotion/react";
import background from "../../img/library-background.png";

const HomePage = () => {
  return (
    <div
      css={css({
        width: "100vw",
        height: "100vh",
        backgroundImage: `url(${background})`,
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
      })}
    >
      <div
        css={css({
          width: "50%",
          height: "100%",
          textAlign: "center",
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          backgroundColor: "rgba(5,5,5,0.6)",
        })}
      >
        <h1 css={css({ color: "#1976d2", fontSize: "80px" })}>
          Welcome To "Ma'ale Nifgaim" Library
        </h1>
      </div>
    </div>
  );
};

export default HomePage;
