import { React, useEffect, useState } from "react";
import AddIcon from "@mui/icons-material/Add";
import { Link } from "react-router-dom";
import axios from "axios";
import ImgList from "../generic/ImgList";
import Modal from "../generic/BasicModal";
import { useForm } from "react-hook-form";
import {
  FormControl,
  Input,
  Button,
  CircularProgress,
  Box,
} from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
/** @jsxImportSource @emotion/react */
import { css } from "@emotion/react";
import Snackbar from "../generic/Snackbar";
import Dialog from "../generic/Dialog";
import SearchBar from "../generic/SearchBar";

const Books = () => {
  const [books, setBooks] = useState([]);
  const [selectedBookId, setSelectedBookId] = useState("");
  const [isLoading, setIsLoading] = useState(true);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [modalTitle, setModalTitle] = useState("");
  const [modalContent, setModalContent] = useState();
  const [isSnackBarOpen, setIsSnackBarOpen] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState();
  const [snackbarSeverity, setSnackbarSeverity] = useState();
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const [dialogContent, setDialogContent] = useState();
  const [dialogTitle, setDialogTitle] = useState();
  const [searchQuery, setSearchQuery] = useState("");

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  useEffect(() => {
    getAllBooks();
  }, []);

  useEffect(() => {
    if (searchQuery == "") {
      getAllBooks();
    } else {
      filterBooks();
    }
  }, [searchQuery]);

  const getAllBooks = async () => {
    let res = await axios.get("http://localhost:1234/book");
    setBooks(res.data);
    setIsLoading(false);
  };

  const booksToDisplay = books.map((book) => ({
    id: book.book_id,
    img: book.cover_img_url,
    title: book.name,
    description: `${book.LibPublisher.first_name}  ${book.LibPublisher.last_name}`,
  }));

  const deleteBook = async () => {
    try {
      let res = await axios.delete(
        `http://localhost:1234/book/${selectedBookId}`
      );
      closeDialog();
      setIsLoading(true);
      getAllBooks();
      setSnackbarMessage(`${res.data.name} deleted`);
      setSnackbarSeverity("success");
    } catch (err) {
      setSnackbarMessage(err.response.data || err.message);
      setSnackbarSeverity("error");
    } finally {
      closeDialog();
      openSnackbar();
    }
  };

  const openModal = (event) => {
    setIsModalOpen(true);
  };

  const clodeModal = () => {
    setIsModalOpen(false);
  };

  const openDialog = () => {
    setIsDialogOpen(true);
  };

  const closeDialog = () => {
    setIsDialogOpen(false);
  };

  const closeSnackBar = () => {
    setIsSnackBarOpen(false);
  };

  const openSnackbar = () => {
    setIsSnackBarOpen(true);
  };

  const filterBooks = async () => {
    try {
      let res = await axios.get(
        `http://localhost:1234/book/search/${searchQuery}`
      );
      setBooks(res.data);
    } catch (err) {
      setSnackbarMessage(err.response.data);
      setSnackbarSeverity("error");
      openSnackbar();
    }
  };

  const showBestSellers = async (data) => {
    const count = data.topCount;
    let res = await axios.get(
      `http://localhost:1234/book/bestSellers/${count}`
    );
    setModalTitle(`Top ${count} Best Sellers Books`);
    setModalContent(
      res.data.map((book) => (
        <h4>
          "{book.name}" was Borrowed {book.borrowsCount} times
        </h4>
      ))
    );
    openModal();
  };

  const handleDelete = async (event) => {
    setSelectedBookId(event.currentTarget.id);
    setDialogTitle("Are you sure?");
    const book = await books.find(book=> book.book_id == event.currentTarget.id);
    setDialogContent(`delete  ${book.name}?`);
    openDialog();
  };

  const handleClick = async (event) => {
    const index = event.currentTarget.id;
    const book = books[index];
    const copies = await axios.get(
      `http://localhost:1234/book/copies/${book.book_id}`
    );
    setModalTitle(book.name);
    setModalContent(`number of copies: ${copies.data}`);
    openModal();
  };

  const deleteAction = {
    icon: DeleteIcon,
    text: "delete",
    action: handleDelete,
  };

  return (
    <div
      css={css({
        display: "flex",
        flexDirection: "column",
        flexWrap: "wrap",
        justifyContent: "top",
        alignItems: "center",
        padding: "100px",
      })}
    >
      <h1>Books</h1>
      {isLoading && (
        <Box sx={{ display: "flex" }}>
          <CircularProgress />
        </Box>
      )}
      {!isLoading && (
        <div>
          <SearchBar setSearchQuery={setSearchQuery} />
          <div>
            <ImgList
              clickFunction={handleClick}
              data={booksToDisplay}
              actionIcon={deleteAction}
            />
            <form
              onSubmit={handleSubmit(showBestSellers)}
              css={css({
                margin: 10,
                top: "10%",
                left: "3%",
                position: "fixed",
                backgroundColor: "white",
                padding: "7px",
              })}
            >
              <p>Top</p>
              <FormControl variant="filled" fullWidth>
                <Input
                  fullWidth
                  id="topCount"
                  {...register("topCount", {
                    required: "required",
                    pattern: {
                      value: /^[0-9]+$/,
                      message: "input must be positive integer",
                    },
                  })}
                />
                {errors.topCount != null && (
                  <p css={css({ color: "red" })}> {errors.topCount?.message}</p>
                )}
              </FormControl>
              <p>Best Sellers</p>
              <FormControl>
                <Button variant="contained" type="submit">
                  Show
                </Button>
              </FormControl>
            </form>
            {/* modal.open &&  */}
            {
              <Modal
                title={modalTitle}
                content={modalContent}
                open={isModalOpen}
                // handleOpen={openModal}
                handleClose={clodeModal}
              />
            }
          </div>
        </div>
      )}
      <Snackbar
        // props={snackbar}
        message={snackbarMessage}
        open={isSnackBarOpen}
        handleClose={closeSnackBar}
        severity={snackbarSeverity}
      />
      <Dialog
        // props={dialog}
        title={dialogTitle}
        content={dialogContent}
        open={isDialogOpen}
        handleClose={closeDialog}
        handleOK={deleteBook}
        handleCancel={closeDialog}
      />
      <Button
        component={Link}
        to="/newBook"
        variant="contained"
        startIcon={<AddIcon />}
        css={css({
          margin: 10,
          bottom: "7%",
          left: "3%",
          position: "fixed",
        })}
      >
        New Book
      </Button>
    </div>
  );
};

export default Books;
