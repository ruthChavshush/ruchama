import React, { useEffect, useState } from "react";
import { Snackbar, Slide } from "@mui/material";
import MuiAlert from '@mui/material/Alert'


const TransitionRight = (props) => {
  return <Slide {...props} direction="right" />;
};

const DirectionSnackba=(props)=>{
  const [transition, setTransition] = useState(undefined);

  useEffect(() => {
    setTransition(() => TransitionRight);
  }, []);

  const Alert = React.forwardRef(function Alert(props, ref) {
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
  });

  return (
    <div>
      <Snackbar
        open={props.open}
        autoHideDuration={6000}
        onClose={props.handleClose}
        TransitionComponent={transition}
        key={transition ? transition.name : ""}
      >
        <Alert
          onClose={props.handleClose}
          severity={props.severity}
          sx={{ width: "100%" }}
        >
          {props.message}
        </Alert>
      </Snackbar>
    </div>
  );
};

export default DirectionSnackba;
