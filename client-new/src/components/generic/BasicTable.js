import { React, useEffect, useState } from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  IconButton,
  Tooltip,
} from "@mui/material";
/** @jsxImportSource @emotion/react */
import { css } from "@emotion/react";

const BasicTable = (props) => {
  const [rows, setRows] = useState([]);
  const headers = props.headers.map((prop) => prop["displayName"]);

  useEffect(() => {
    setRows(props.rows);
  }, []);

  const getCellContent = (row, header) => {
    let content;
    switch (header.type) {
      case "txt":
        content = row[header.realName];
        break;
      case "img":
        content = <img src={header.realName} alt={header.realName}></img>;
        break;
      default:
        content = null;
        break;
    }
    return content;
  };

  const rowToDisplay = (row) => {
    return props.headers.map((header, index) => (
      <TableCell key={index} align="center">
        {getCellContent(row, header)}
      </TableCell>
    ));
  };

  const headersToDisplay = headers.map((header) => (
    <TableCell key={header} align="center">
      <strong>{header}</strong>
    </TableCell>
  ));

  const actions = (row) => {
    return (
      <TableCell align="center">
        {props.actions.map((action) => (
          <Tooltip title={action.text}>
            <IconButton
              id={row.id}
              size="large"
              edge="start"
              color="inherit"
              cursor="pointer"
              aria-label="menu"
              sx={{ mr: 2 }}
              onClick={async (event) => {
                event.stopPropagation();
                await action.action(event);
              }}
            >
              {<action.icon />}
            </IconButton>
          </Tooltip>
        ))}
      </TableCell>
    );
  };

  let rowsToDisplay = rows.map((row) => (
    <TableRow
      key={row.id}
      sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
    >
      {rowToDisplay(row)}
      {actions(row)}
    </TableRow>
  ));

  return (
    <div>
      {rows.length > 0 && (
        <TableContainer sx={{ width: "50vw", minWidth: 500 }} component={Paper}>
          <Table stickyHeader sx={{ minWidth: 300 }} aria-label="simple table">
            <TableHead>
              <TableRow>
                {headersToDisplay}
                <TableCell key={headers.length} align="center">
                  <strong>Actions</strong>
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>{rowsToDisplay}</TableBody>
          </Table>
        </TableContainer>
      )}
      {rows.length == 0 && <p>no data</p>}
    </div>
  );
};

export default BasicTable;
