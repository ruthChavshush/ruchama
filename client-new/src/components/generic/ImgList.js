import React, { useState, useEffect } from "react";
import {
  ImageList,
  ImageListItem,
  ImageListItemBar,
  IconButton,
  Tooltip,
} from "@mui/material";

const ImgList = (props) => {

  const data = props.data.map((item, index) => (
    <ImageListItem
      onClick={props.clickFunction}
      key={index}
      id={index}
    >
      <img
        src={`${item.img}?w=248&fit=crop&auto=format`}
        srcSet={`${item.img}?w=248&fit=crop&auto=format&dpr=2 2x`}
        alt={item.title}
        subtitle={item.description}
        loading="lazy"
      />
      <ImageListItemBar
        title={item.title}
        subtitle={item.description}
        actionIcon={
          <Tooltip title={props.actionIcon.text}>
            <IconButton
              id={item.id}
              size="large"
              edge="start"
              color="primary"
              cursor="pointer"
              aria-label="menu"
              sx={{ mr: 2 }}
              onClick={async (event) => {
                event.stopPropagation();
                await props.actionIcon.action(event);
              }}
            >
              {<props.actionIcon.icon />}
            </IconButton>
          </Tooltip>
        }
      />
    </ImageListItem>
  ));

  return (
    <div>
      <ImageList sx={{ width: "30vw", minWidth: 300 }}>{data}</ImageList>{" "}
    </div>
  );
};

export default ImgList;
