import * as React from "react";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  useMediaQuery,
  Box
} from "@mui/material";
import ErrorIcon from '@mui/icons-material/Error';
import HelpOutlineIcon from "@mui/icons-material/HelpOutline";
import { useTheme } from "@mui/material/styles";

const ResponsiveDialog = (props) => {
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down("md"));

  return (
    <div>
      <Dialog
        fullScreen={fullScreen}
        open={props.open}
        onClose={props.handleClose}
        aria-labelledby="responsive-dialog-title"
      >
        {/* <DialogTitle id="responsive-dialog-title"> */}
        <DialogContent>
          <Box
           sx={{ textAlign: 'center'}}
          //   sx={{
          //     "& > :not(style)": {
          //       m: 2,
          //     },
          //   }}
          // >
          >
            <ErrorIcon color="warning" fontSize="large"/>
            {/* <HelpOutlineIcon color="warning" fontSize="large"/> */}
          </Box>
          </DialogContent>
        {/* </DialogTitle> */}
        <DialogTitle id="responsive-dialog-title">{props.title}</DialogTitle>
        <DialogContent>
          <DialogContentText>{props.content}</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button variant="contained" color="success" autoFocus onClick={props.handleOK}>
            ok
          </Button>
          <Button variant="outlined" color="error" onClick={props.handleCancel} autoFocus>
            cancel
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};
export default ResponsiveDialog;
