import React, { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import {
  FormControl,
  InputLabel,
  Input,
  Button,
  MenuItem,
  Select,
} from "@mui/material";
import axios from "axios";
/** @jsxImportSource @emotion/react */
import { css } from "@emotion/react";
import { useNavigate } from "react-router-dom";
import Snackbar from "../generic/Snackbar";

const NewBook = () => {
  const [publishers, setPublishers] = useState([]);
  const [publisherId, setPublisherId] = useState();
  const [isSnackBarOpen, setIsSnackBarOpen] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState();
  const [snackbarSeverity, setSnackbarSeverity] = useState();

  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm();

  useEffect(() => {
    gatAllPublishers();
  }, []);

  const openSnackBar = () => {
    setIsSnackBarOpen(true);
  };

  const closeSnackBar = () => {
    setIsSnackBarOpen(false);
  };

  const gatAllPublishers = async () => {
    let res = await axios.get("http://localhost:1234/publisher");
    setPublishers(res.data);
  };

  const addBook = async (book) => {
    try {
      let res = await axios.put("http://localhost:1234/book", book);
      setSnackbarMessage(`${res.data.name} added succesfuly`);
      setSnackbarSeverity("success");
      openSnackBar();
      reset();
    } catch (err) {

      setSnackbarMessage( err.message || err.response.data );
      setSnackbarSeverity("error");
      openSnackBar();
    }
  };

  const onSubmit = (data) => {
    addBook(data);
  };

  const publishersToDisplay = publishers.map((publisher) => (
    <MenuItem
      onClick={() => {
        setPublisherId(publisher.id);
      }}
      key={publisher.id}
      value={publisher.id}
    >{`${publisher.first_name} ${publisher.last_name}`}</MenuItem>
  ));

  return (
    <div>
      <form
        onSubmit={handleSubmit(onSubmit)}
        css={css({
          margin: "150px",
          padding: "30px",
          width: "30vw",
          minWidth: "200px",
          display: "flex",
          flexDirection: "column",
          flexWrap: "wrap",
          justifyContent: "top",
          backgroundColor: "white",
          alignItems: "center",
        })}
      >
        <h1>New Book</h1>
        <FormControl variant="filled" fullWidth>
          <InputLabel htmlFor="name">Book Name</InputLabel>
          <Input
            fullWidth
            id="name"
            {...register("bookName", {
              required: "book name is required",
              maxLength: {
                value: 30,
                message: "book name must be at most 30 characters long",
              },
            })}
          />
          <p css={css({ color: "red" })}> {errors.bookName?.message}</p>
        </FormControl>
        <FormControl variant="filled" fullWidth>
          <InputLabel htmlFor="publisherId">Author</InputLabel>
          <Select
            labelId="bookId"
            id="publisherId"
            value={publisherId}
            label="Book"
            {...register("PublisherId", {
              required: "choose author",
            })}
          >
            {publishersToDisplay}
          </Select>
          <p css={css({ color: "red" })}> {errors.PublisherId?.message}</p>
        </FormControl>
        <FormControl variant="filled" fullWidth>
          <InputLabel htmlFor="cover">Cover Image Url</InputLabel>
          <Input
            fullWidth
            id="cover"
            {...register("CoverImageUrl", {
              required: "cover image url is required",
            })}
          />
          <p css={css({ color: "red" })}> {errors.CoverImageUrl?.message}</p>
        </FormControl>
        <FormControl variant="filled" fullWidth>
          <InputLabel htmlFor="price">Price</InputLabel>
          <Input
            fullWidth
            id="price"
            {...register("price", {
              required: "price is required",
              pattern: {
                value: /^[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)$/,
                message: "input must be number",
              },
            })}
          />
          <p css={css({ color: "red" })}> {errors.price?.message}</p>
        </FormControl>
        <FormControl variant="filled" fullWidth>
          <InputLabel htmlFor="copies">Copies Numbers</InputLabel>
          <Input
            fullWidth
            id="copies"
            {...register("CopiesNumber", {
              required: "copies number is required",
              pattern: {
                value: /^[0-9]+$/,
                message: "input must be positive integer",
              },
            })}
          />
          <p css={css({ color: "red" })}> {errors.CopiesNumber?.message}</p>
        </FormControl>
        <FormControl>
          <Button variant="contained" type="submit">
            Add
          </Button>
        </FormControl>
      </form>
      <Snackbar
        message={snackbarMessage}
        open={isSnackBarOpen}
        handleClose={closeSnackBar}
        severity={snackbarSeverity}
      />
    </div>
  );
};

export default NewBook;
