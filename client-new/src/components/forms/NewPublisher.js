import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { FormControl, InputLabel, Input, Button } from "@mui/material";
import axios from "axios";
import Snackbar from "../generic//Snackbar";
/** @jsxImportSource @emotion/react */
import { css } from "@emotion/react";

const NewPublisher = () => {
  const [isSnackBarOpen, setIsSnackBarOpen] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState();
  const [snackbarSeverity, setSnackbarSeverity] = useState();
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm();

  const addPublisher = async (publisher) => {
    try {
      let res = await axios.put("http://localhost:1234/publisher", publisher);
      setSnackbarMessage(
        `${res.data.first_name} ${res.data.last_name} added succesfuly`
      );
      setSnackbarSeverity("success");
      reset();
    } catch (err) {
      setSnackbarMessage(err.response.data || err.message);
      setSnackbarSeverity("error");
    } finally {
      openSnackBar();
    }
  };

  const openSnackBar = () => {
    setIsSnackBarOpen(true);
  };

  const closeSnackBar = () => {
    setIsSnackBarOpen(false);
  };

  const onSubmit = (data) => {
    addPublisher(data);
  };

  return (
    <div>
      <form
        onSubmit={handleSubmit(onSubmit)}
        css={css({
          margin: "150px",
          padding: "30px",
          width: "30vw",
          backgroundColor: "white",
          minWidth: "200px",
          display: "flex",
          flexDirection: "column",
          flexWrap: "wrap",
          justifyContent: "top",
          alignItems: "center",
        })}
      >
        <h1>New Publisher</h1>
        <FormControl variant="filled" fullWidth>
          <InputLabel htmlFor="id">ID Number</InputLabel>
          <Input
            id="id"
            {...register("id", {
              required: "id number is required",
              pattern: {
                value: /^[0-9]+$/,
                message: "digits only",
              },
              minLength: {
                value: 9,
                message: "length must be 9",
              },
              maxLength: {
                value: 9,
                message: "length must be 9",
              },
            })}
          />
          <p css={css({ color: "red" })}> {errors.id?.message}</p>
        </FormControl>
        <FormControl variant="filled" fullWidth>
          <InputLabel htmlFor="first-name">First Name</InputLabel>
          <Input
            id="first-name"
            {...register("firstName", { required: "first name is required" })}
          />
          <p css={css({ color: "red" })}> {errors.firstName?.message}</p>
        </FormControl>
        <FormControl variant="filled" fullWidth>
          <InputLabel htmlFor="last-name">Last Name</InputLabel>
          <Input
            id="last-name"
            {...register("lastName", { required: "last name is required" })}
          />
          <p css={css({ color: "red" })}> {errors.lastName?.message}</p>
        </FormControl>
        <FormControl variant="filled">
          <Button variant="contained" type="submit">
            Add
          </Button>
        </FormControl>
      </form>
      <Snackbar
        message={snackbarMessage}
        open={isSnackBarOpen}
        handleClose={closeSnackBar}
        severity={snackbarSeverity}
      />
    </div>
  );
};

export default NewPublisher;
