import { React, useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import {
  FormControl,
  InputLabel,
  Input,
  Button,
  MenuItem,
  Select,
} from "@mui/material";
import axios from "axios";
/** @jsxImportSource @emotion/react */
import { css } from "@emotion/react";
import Snackbar from "../generic/Snackbar";

const NewBorrow = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm();

  const [bookId, setBookId] = useState();
  const [books, setBooks] = useState([]);
  const [isSnackBarOpen, setIsSnackBarOpen] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState();
  const [snackbarSeverity, setSnackbarSeverity] = useState();

  useEffect(() => {
    getAllBooks();
  }, []);

  const openSnackBar = () => {
    setIsSnackBarOpen(true);
  };

  const closeSnackBar = () => {
    setIsSnackBarOpen(false);
  };

  const onSubmit = (data) => {
    addBorrow(data);
  };

  const getAllBooks = async () => {
    try {
      let res = await axios.get("http://localhost:1234/book");
      setBooks(res.data);
    } catch (err) {
      setSnackbarMessage(err.response.data || err.message);
      setSnackbarSeverity("error");
      openSnackBar();
    }
  };

  const addBorrow = async (borrow) => {
    try {
      let res = await axios.put("http://localhost:1234/borrow", borrow);
      setSnackbarMessage(
        `${res.data.LibReader.first_name} ${res.data.LibReader.last_name}'s borrow added`
      );
      setSnackbarSeverity("success");
      reset();
    } catch (err) {
      setSnackbarMessage(err.response.data || err.message);
      setSnackbarSeverity("error");
    } finally {
      openSnackBar();
    }
  };

  const booksToDisplay = books.map((book) => (
    <MenuItem
      onClick={() => {
        setBookId(book.book_id);
      }}
      key={book.book_id}
      value={book.book_id}
    >
      {" "}
      {book.name}
    </MenuItem>
  ));

  return (
    <div>
      <form
        onSubmit={handleSubmit(onSubmit)}
        css={css({
          margin: "150px",
          padding: "30px",
          backgroundColor: "white",
          width: "30vw",
          minWidth: "200px",
          display: "flex",
          flexDirection: "column",
          flexWrap: "wrap",
          justifyContent: "top",
          alignItems: "center",
        })}
      >
        <h1>New Borrow</h1>
        <FormControl variant="filled" fullWidth>
          <InputLabel htmlFor="readerId">Reader ID Number</InputLabel>
          <Input
            fullWidth
            id="readerId"
            {...register("readerId", {
              required: "Reader ID Number is required",
            })}
          />
          <p css={css({ color: "red" })}> {errors.readerId?.message}</p>
        </FormControl>
        <FormControl variant="filled" fullWidth>
          <InputLabel htmlFor="bookId">Book</InputLabel>
          <Select
            labelId="bookId"
            id="bookId"
            value={bookId}
            label="Book"
            {...register("bookId", {
              required: "choose book",
            })}
          >
            {booksToDisplay}
          </Select>
          <p css={css({ color: "red" })}> {errors.bookId?.message}</p>
        </FormControl>
        <FormControl>
          <Button variant="contained" type="submit">
            Add
          </Button>
        </FormControl>
      </form>
      <Snackbar
        message={snackbarMessage}
        open={isSnackBarOpen}
        handleClose={closeSnackBar}
        severity={snackbarSeverity}
      />
    </div>
  );
};

export default NewBorrow;
