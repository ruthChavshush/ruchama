import Navbar from "./components/Navbar";
import { React } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Books from "./components/pages/Books";
import Readers from "./components/pages/Readers";
import Borrows from "./components/pages/Borrows";
import HomePage from "./components/pages/HomePage";
import Publishers from "./components/pages/Publishers";
import NewBook from "./components/forms/NewBook";
import NewReader from "./components/forms/NewReader";
import NewPublisher from "./components/forms/NewPublisher";
import NewBorrow from "./components/forms/NewBorrow";
import background from "./img/library-background.png";
/** @jsxImportSource @emotion/react */
import { css } from "@emotion/react";

export default function App(){
  return (
    <Router>
      <div
        className="App"
        css={css({
          display: "flex",
          flexDirection: "column",
          flexWrap: "wrap",
          justifyContent: "top",
          alignItems: "center",
          minHeight: "100vh",
          backgroundImage: `url(${background})`,
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover",
          backgroundAttachment: "fixed",
        })}
      >
        <Navbar />
        <Routes>
          <Route exact path="/" element={<HomePage />}></Route>
          <Route exact path="/readers" element={<Readers />}></Route>
          <Route exact path="/borrows" element={<Borrows></Borrows>}></Route>
          <Route
            exact
            path="/publishers"
            element={<Publishers></Publishers>}
          ></Route>
          <Route exact path="/books" element={<Books></Books>}></Route>
          <Route exact path="/newBook" element={<NewBook />}></Route>
          <Route exact path="/newReader" element={<NewReader />}></Route>
          <Route exact path="/newPublisher" element={<NewPublisher />}></Route>
          <Route exact path="/newBorrow" element={<NewBorrow />}></Route>
        </Routes>
      </div>
    </Router>
  );
};


